2020-10-26 André Silva <andre.silva.programador@gmail.com>
* Version: 0.1.12
* Bind.cs: Cleanup
* CommandAction.cs: refactory, cleanup, New CommandEventInfo Class
* EventInfo.cs: refactory
* ObserverEventSystem.cs: cleanup, onlyOnceInstance implemented
* TimeTickSystem.cs: cleanup;
* Version.cs: Cleanup
* Editor: Added some editor snippets;

2020-06-30 André Silva <andre.silva.programador@gmail.com>
* Version: 0.1.11
* CommandAction.cs: CommandAction & CommandActionList Implementation;

2020-06-14 André Silva <andre.silva.programador@gmail.com>
* Version: 0.1.10
* Removido e ignorado meta files do git, causava aviso no Unity;

2020-06-13 André Silva <andre.silva.programador@gmail.com>
* Version: 0.1.9
* FHUtility.cs: Sobrecarregado metodo GetScreenWorld2D com padrão do Camera.main;

2020-06-02 André Silva <andre.silva.programador@gmail.com>
* Version: 0.1.8
* EventInfo.cs: Done Implementation;
* FHStatePattern.cs: cleanup;
* FHUtility.cs: 0 duration bugfix;
* ObserverEventSystem.cs: cleanup;
* StateMachine.cs: cleanup;

2020-05-17 André Silva <andre.silva.programador@gmail.com>
* Version: 0.1.7
* StateMachine.cs: FiniteStateMachine rework;
* FHStatePattern: State Pattern implementation;

2020-04-27 André Silva <andre.silva.programador@gmail.com>
* Version: 0.1.6
* StateMachine.cs: StateMachine definition and implementation;
				   Interface IState;

2020-04-17 André Silva <andre.silva.programador@gmail.com>
* EventInfo.cs: UnitEventInfo specialization;
* FHUtility.cs: UpdateTimer modified to use UnitObserverEventSystem;
* ObserverEventSystem.cs: UnitObserverEventSystem implementation;
* SharedInterfaces.cs: IUnit removed all methods;

2020-03-26 André Silva <andre.silva.programador@gmail.com>
* EventInfo.cs: Instance TriggerEvent method;
* SharedInterfaces.cs: IUnit - TriggerEvent modified to accept eventInfo object;

2020-03-16 André Silva <andre.silva.programador@gmail.com>
* FHUtility.cs: Metodos para converter posicoes para WorldPoint;
* ObserverEventSystem.cs: UnitObserverEventSystem, para dar mais flexibilidade e identificar instancias de objetos para acionar eventos;
* SharedInterfaces.cs: IUnit - Interface utilizada pelo Observer para identificar instancias;
* EventInfo.cs: Refatoração

2020-02-12 André Silva <andre.silva.programador@gmail.com>
* ObserverEventSystem.cs: Functionalidade experimental TryTrigger (Queue)
* FHUtility.cs: Assinatura do UpdateTimer.Create criada para gerar timer sem updateCallback;

2020-02-11 André Silva <andre.silva.programador@gmail.com>
* FHUtility.cs: Refatoração do UpdateTimer, adicionado funcionalidade de remover e cancelar o timer.
Removido suporte a callback Completed no UpdateTimer, com a refatoriacao o evento se tornou obsoleto por que o Hook apenas chama aciona o gatilho para o ObserverSystem e cada instancia de UpdateTimer se auto-gerencia;
