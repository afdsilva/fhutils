using UnityEngine;
using UnityEditor;
using System;
using System.Reflection;
using System.Collections.Generic;

namespace FourHands {
    public static class MyExtensions {
        public static object GetValue(this UnityEditor.SerializedProperty property) {
            object obj = property.serializedObject.targetObject;

            FieldInfo field = null;
            foreach (var path in property.propertyPath.Split('.')) {
                var type = obj.GetType();
                field = type.GetField(path);
                obj = field.GetValue(obj);
            }
            return obj;
        }
        public static void SetValue(this UnityEditor.SerializedProperty property, object val) {
            object obj = property.serializedObject.targetObject;

            List<KeyValuePair<FieldInfo, object>> list = new List<KeyValuePair<FieldInfo, object>>();

            FieldInfo field = null;
            foreach (var path in property.propertyPath.Split('.')) {
                var type = obj.GetType();
                field = type.GetField(path);
                list.Add(new KeyValuePair<FieldInfo, object>(field, obj));
                obj = field.GetValue(obj);
            }

            // Now set values of all objects, from child to parent
            for (int i = list.Count - 1; i >= 0; --i) {
                list[i].Key.SetValue(list[i].Value, val);
                // New 'val' object will be parent of current 'val' object
                val = list[i].Value;
            }
        }
    }

    [CustomPropertyDrawer(typeof(Version))]
    public class VersionPropertyDrawer : PropertyDrawer {
        bool edit;
        string version;
        public override void OnGUI(Rect pos, SerializedProperty prop, GUIContent label) {
            float row1 = pos.height / 2;
            EditorGUI.LabelField(
             new Rect(pos.x, pos.y, pos.width, row1),
             label.text);
            SerializedProperty majorVersion = prop.FindPropertyRelative("majorVersion");
            SerializedProperty minorVersion = prop.FindPropertyRelative("minorVersion");
            SerializedProperty dateVersion = prop.FindPropertyRelative("dateVersion");
            SerializedProperty buildVersion = prop.FindPropertyRelative("buildVersion");
            SerializedProperty buildVersionType = prop.FindPropertyRelative("buildVersionType");

            buildNumberProperty(new Rect(pos.x + 50, pos.y, 45, row1), majorVersion);
            buildNumberProperty(new Rect(pos.x + 100, pos.y, 45, row1), minorVersion);
            buildDateProperty(new Rect(pos.x + 150, pos.y, 45, row1), dateVersion);
            buildVersionProperty(new Rect(pos.x, pos.y + row1, 150, row1), buildVersion, buildVersionType);
            Version currentVersion = (Version)prop.GetValue();
            if (edit) {
                version = EditorGUI.TextField(new Rect(pos.x + 210, pos.y + row1, 150, row1), version);
            } else {
                EditorGUI.LabelField(new Rect(pos.x + 210, pos.y + row1, 150, row1), currentVersion?.ToString());
            }
            if (GUI.Button(new Rect(pos.x + 360, pos.y + row1, 30, row1), edit == false ? "Edit" : "Save")) {
                edit = !edit;
                if (edit) {
                    version = currentVersion?.ToString();
                } else {
                    if (version != currentVersion.ToString()) {
                        if (Version.TryParse(version, out Version newVersion)) 
                            prop.SetValue(newVersion);
                    }
                }
            }
        }
        float buttonWidth = 15f;
        float textWidth = 15f;
        void buildNumberProperty(Rect rect, SerializedProperty prop) {
            if (GUI.Button(new Rect(rect.x, rect.y, buttonWidth, rect.height), "+")) prop.intValue++;
            EditorGUI.LabelField(new Rect(rect.x + buttonWidth + 3, rect.y, textWidth, rect.height), prop.intValue.ToString());
            if (GUI.Button(new Rect(rect.x + buttonWidth + textWidth, rect.y, buttonWidth, rect.height), "-")) if (prop.intValue > 0) prop.intValue--;
        }
        float dateWidth = 70f;
        float buttonDateWidth = 60f;
        void buildDateProperty(Rect rect, SerializedProperty prop) {
            prop.intValue = int.Parse(
                String.Format("{0:yyyyMMdd}",
                EditorGUI.TextField(new Rect(rect.x, rect.y, dateWidth, rect.height),
                prop.intValue.ToString())));
            if (GUI.Button(new Rect(rect.x + dateWidth, rect.y, buttonDateWidth, rect.height), "Now")) {
                prop.intValue = int.Parse(String.Format("{0:yyyyMMdd}", DateTime.Now));
                //if (EditorUtility.DisplayDialog("Warning", "This action will override the current value", "Yes", "Cancel")) {
                //}
            }
            //
        }
        float labelVersionTypeWidth = 50f;
        float enumVersionTypeWidth = 100f;
        void buildVersionProperty(Rect rect, SerializedProperty buildVersion, SerializedProperty buildVersionType) {
            EditorGUI.LabelField(new Rect(rect.x, rect.y, labelVersionTypeWidth, rect.height), "Build");
            buildVersionType.enumValueIndex = EditorGUI.Popup(new Rect(rect.x + labelVersionTypeWidth, rect.y, enumVersionTypeWidth, rect.height), buildVersionType.enumValueIndex, buildVersionType.enumNames);
            buildNumberProperty(new Rect(rect.x + labelVersionTypeWidth + enumVersionTypeWidth, rect.y, buttonWidth, rect.height), buildVersion);
        }
        
        public override float GetPropertyHeight(SerializedProperty prop,
                                              GUIContent label) {
            return base.GetPropertyHeight(prop, label) * 2;

        }

    }
}