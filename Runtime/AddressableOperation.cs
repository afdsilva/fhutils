﻿using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using System;
using System.Collections;
using System.Collections.Generic;

namespace FourHands {
    public abstract class AddressableOperation : IEnumerator {
        public AddressableOperation() {
            Error = string.Empty;
        }
        public delegate void ProgressDelegate(string label, float value);

        /// <summary>
        /// Disable the warning never used bullshit
        /// </summary>
        #pragma warning disable 67
        public virtual event ProgressDelegate OnUpdateProgress;
        #pragma warning restore 67

        public string Error { get; protected set; }
        public object Current { get { return null; } }
        abstract public bool Update();
        public bool MoveNext() {
            if (IsDone) {
                return false;
            }
            return true;
        }
        public abstract bool IsDone { get; }
        public abstract float PercentComplete { get; }
        public virtual void Reset() { }
    }
    public class RequestAddressableAsset<T> : AddressableOperation {
        public RequestAddressableAsset(AssetReference assetReference, string label, Action<AsyncOperationHandle<T>> callback) {
            AssetReference = assetReference;
            Label = label;
            completedCallback = callback;
        }
        protected AsyncOperationHandle<T> operationHandler;
        protected Action<AsyncOperationHandle<T>> completedCallback;
        public override event ProgressDelegate OnUpdateProgress;
        public AssetReference AssetReference { get; protected set; }
        public string Label { get; protected set; }
        bool completed;
        public override bool IsDone {
            get { return operationHandler.IsDone && completed; }
        }
        public override float PercentComplete { get { return (operationHandler.IsValid() == false ? -1 : operationHandler.PercentComplete); } }

        bool init;
        public override bool Update() {
            if (init == false) {
                init = true;
                operationHandler = Addressables.LoadAssetAsync<T>(AssetReference);
                completed = false;
                operationHandler.Completed += completedCallback;
                operationHandler.Completed += (AsyncOperationHandle<T> obj) => { completed = true; };
            }
            OnUpdateProgress?.Invoke(Label, PercentComplete);

            return !IsDone;
        }
    }
    public class RequestAddressableCollection<T> : AddressableOperation {
        public RequestAddressableCollection(string group, string label, Action<T> callback) {
            Group = group;
            Label = label;
            this.callback = callback;
        }
        Action<T> callback;
        AsyncOperationHandle<IList<T>> operationHandle;
        public override event ProgressDelegate OnUpdateProgress;
        bool completed;
        public string Group { get; protected set; }
        public string Label { get; protected set; }
        public override bool IsDone { get { return operationHandle.IsDone && completed; } }
        public override float PercentComplete { get { return operationHandle.IsValid() ? operationHandle.PercentComplete : 0; } }
        bool init;
        public override bool Update() {
            if (init == false) {
                init = true;
                operationHandle = Addressables.LoadAssetsAsync<T>(Group, callback);
                operationHandle.Completed += (AsyncOperationHandle<IList<T>> obj) => { completed = true; };
            }
            OnUpdateProgress?.Invoke(Label, PercentComplete);
            return !IsDone;
        }
    }

}