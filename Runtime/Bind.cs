﻿using UnityEngine;
using System;

namespace FourHands {
    public class Bind {
        public Bind(KeyCode trigger, Condition condition) {
            Trigger = trigger;
            Conditions = condition;
            MouseButtonTrigger = -1;
        }
        public Bind(KeyCode trigger) {
            Trigger = trigger;
            Conditions = Condition.Press;
            MouseButtonTrigger = -1;
        }
        public Bind(int mouseButtonTrigger, Condition condition) {
            Trigger = KeyCode.None;
            MouseButtonTrigger = mouseButtonTrigger;
            Conditions = condition;
        }
        public Bind(int mouseButtonTrigger) {
            Trigger = KeyCode.None;
            MouseButtonTrigger = mouseButtonTrigger;
            Conditions = Condition.Press;
        }
        public bool Same(Bind other) {
            if (other.Trigger != Trigger || other.MouseButtonTrigger != MouseButtonTrigger) return false;
            return true;
        }
        public KeyCode Trigger { get; protected set; }
        public int MouseButtonTrigger { get; protected set; }
        public Condition Conditions { get; protected set; }

        [Flags]
        public enum Condition {
            None = 0,
            Press = 1,
            Release = 2,
            Hold = 4
        }
        public void SetFlag(Condition b) {
            Conditions = Conditions | b;
        }
        public void UnsetFlag(Condition b) {
            Conditions = Conditions & (~b);
        }
        public bool HasFlag(Condition b) {
            return (Conditions & b) == b;
        }
        public void ToggleFlag(Condition b) {
            Conditions = Conditions ^ b;
        }
        public bool Compare(Bind other) {
            if (Trigger == other.Trigger)
                return false;
            if (Conditions == other.Conditions)
                return false;
            return true;
        }
    }
    public class BindInfo {
        protected BindInfo() { }
        public BindInfo(string source, Bind[] binds, Action action, Func<BindInfo, bool> allowedUsageCheck = null) {
            Source = source;
            Binds = binds;
            Action = action;
            AllowedUsageCheck = allowedUsageCheck;
        }
        public Bind[] Binds { get; protected set; }
        public Action Action { get; protected set; }
        public Func<BindInfo, bool> AllowedUsageCheck { get; protected set; }
        public string Source { get; protected set; }
        public bool AllowedUsage() {
            if (AllowedUsageCheck == null) return true;
            return AllowedUsageCheck(this);
        }
        public bool IsValid(out string message) {
            message = string.Empty;
            if (Binds == null || Binds.Length <= 0) {
                message = "No binds registered";
                return false;
            }
            if (Action == null) {
                message = "No action registered";
                return false;
            }
            if (string.IsNullOrEmpty(Source)) {
                message = "Bind has no source";
                return false;
            }
            return true;
        }
    }
}