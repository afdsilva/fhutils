﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FourHands {
    public interface ICommand {
        void Execute();
        void Pause();
        void Resume();
        void Skip();
        void Cancel();
        void Complete();
        bool IsDone { get; }
        event Action Completed;
        //event Action<ICommand> ExecuteCallback;
        //event Action<ICommand> PauseCallback;
        //event Action<ICommand> ResumeCallback;
        //event Action<ICommand> SkipCallback;
        //event Action<ICommand> CancelCallback;
    }
    public class CommandAction : ICommand {
        public CommandAction() {
            _executeOnce = true;
        }
        public CommandAction(Action<ICommand> executeCallback = null, Action<ICommand> pauseCallback = null, Action<ICommand> resumeCallback = null, Action<ICommand> skipCallback = null, Action<ICommand> cancelCallback = null, bool executeOnce = true) {
            _executeOnce = executeOnce;
            ExecuteCallback = executeCallback;
            PauseCallback = pauseCallback;
            ResumeCallback = resumeCallback;
            SkipCallback = skipCallback;
            CancelCallback = cancelCallback;
        }
        public event Action Completed;
        public virtual event Action<ICommand> ExecuteCallback;
        public virtual event Action<ICommand> PauseCallback;
        public virtual event Action<ICommand> ResumeCallback;
        public virtual event Action<ICommand> SkipCallback;
        public virtual event Action<ICommand> CancelCallback;

        bool _executeOnce;
        public virtual bool IsDone { get; protected set; }
        bool executing;
        public virtual void Execute() {
            if (executing) return;
            executing = true;
            //nao permite que um comando execute mais de uma vez
            if (IsDone && _executeOnce) return;
            //executa a acao
            ExecuteCallback?.Invoke(this);
        }
        public virtual void Skip() {
            SkipCallback?.Invoke(this);
        }
        public virtual void Cancel() {
            CancelCallback?.Invoke(this);
        }
        public virtual void Pause() {
            PauseCallback?.Invoke(this);
        }
        public virtual void Resume() {
            ResumeCallback?.Invoke(this);
        }
        public virtual void Complete() {
            IsDone = true;
            executing = false;
            Completed?.Invoke();
        }
    }
    public class CommandActionList<T> : IList<T> where T : ICommand {
        private List<T> _innerList;
        public List<T> List { 
            get {
                if (_innerList == null) _innerList = new List<T>();
                return _innerList;
            } 
        }
        bool isExecuting;
        int currentIndex;
        public event Action EndCallback;
        public event Action<T> ExecuteCommandCallback;
        /// <summary>
        /// Executa toda a cadeia de comandos, iniciando no primeiro comando
        /// </summary>
        public void Execute() {
            if (isExecuting) return;
            if (List.Count <= 0) {
                end();
                return;
            }
            isExecuting = true;
            currentIndex = -1; //currentIndex inicia em -1 pois eu quero sempre saber quem é o comando ATUAL
            next();
        }
        /// <summary>
        /// Executa o proximo comando na cadeia, usado internamente
        /// </summary>
        void next() {
            currentIndex++;
            if (currentIndex >= List.Count) {
                end();
                return;
            }
            //Debug.Log($"currentIndex: {currentIndex} - List.Count: {List.Count}");
            T _currentCommand = List[currentIndex];
            if (_currentCommand == null) {
                next();
                return;
            }
            _currentCommand.Completed -= next;
            _currentCommand.Completed += next;
            //executa o comando
            _currentCommand.Execute();
            ExecuteCommandCallback?.Invoke(_currentCommand);
        }
        /// <summary>
        /// Finaliza a cadeia de comandos, usado internamente
        /// </summary>
        void end() {
            isExecuting = false;
            EndCallback?.Invoke();
        }
        /// <summary>
        /// pausa o comando atual e para toda cadeia de comandos
        /// </summary>
        public void Pause() {
            T _currentCommand = List[currentIndex];
            _currentCommand.Pause();
        }
        /// <summary>
        /// Reinicia o comando atual
        /// </summary>
        public void Resume() {
            T _currentCommand = List[currentIndex];
            _currentCommand.Resume();
        }
        /// <summary>
        /// Pula o comando atual
        /// </summary>
        public void Skip() {
            T _currentCommand = List[currentIndex];
            _currentCommand.Skip();
        }
        //pula o comando atual e todos os proximos, porem executando e skipando todos em sequencia
        public void SkipAll() {
            T _currentCommand = List[currentIndex];
            ExecuteCommandCallback -= skipCommand;
            ExecuteCommandCallback += skipCommand;
        } 
        void skipCommand(T command) {
            command.Skip();
        }
        /// <summary>
        /// Cancela o comando atual e nao executa mais nenhum
        /// </summary>
        public void Cancel() {
            T _currentCommand = List[currentIndex];
            _currentCommand.Cancel();
            end();
        }

        public int GetEmptyIndex() {
            int index = List.FindIndex(c => c == null);
            return index;
        }
        #region IList Implementation
        public T this[int index] {
            get {
                return List[index];
            }
            set {
                List[index] = value;
            }
        }
        public int Count {
            get {
                return List.Count;
            }
        }
        public bool IsReadOnly { get { return false; } }
        //adiciona no primeiro espaco nulo ou no final da lista
        public void Add(T item) {
            int firstEmpty = List.FindIndex(t => t == null);
            if (firstEmpty < 0) List.Add(item);
            else List[firstEmpty] = item;
        }
        public void Clear() {
            List.Clear();
        }
        public bool Contains(T item) {
            return List.Contains(item);
        }
        public void CopyTo(T[] array, int arrayIndex) {
            List.CopyTo(array, arrayIndex);

        }
        public IEnumerator<T> GetEnumerator() {
            return List.GetEnumerator();
        }
        public int IndexOf(T item) {
            return List.IndexOf(item);
        }
        //insere na posicao index somente se a posicao estiver vazia
        public void Insert(int index, T item) {
            if (index >= List.Count) {
                var d = index - List.Count;
                var c = new T[d];
                List.AddRange(c);
                List.Insert(index, item);
            } else {
                if (List[index] != null) List.Insert(index, item);
                else List[index] = item;
            }
        }
        public bool Remove(T item) {
            int index = List.IndexOf(item);
            if (index < 0) return false;//nao encontrou
            List[index] = default;
            //remove registros nulos depois do ultimo registro valido
            cleanup();
            return true;
        }
        void cleanup() {
            int index = List.Count - 1;
            while(index >= 0 && List[index] == null) {
                List.RemoveAt(index);
                index--;
            }
        }
        public void RemoveAt(int index) {
            List[index] = default;
            cleanup();
            //List.RemoveAt(index);
        }
        IEnumerator IEnumerable.GetEnumerator() {
            return List.GetEnumerator();
        }
        #endregion
    }

    public interface ICommandEvent {
        void InsertFirst(ICommand command);
        void Insert(ICommand command);
        void InsertLast(ICommand command);
    }
    public abstract class CommandEventInfo : EventInfo {
        public CommandEventInfo() {
            reservedFirst = 0;
            reservedLast = 0;
            if (GetCommands(out CommandActionList<ICommand> commands)) {
                DoneCallback -= commands.Execute;
                DoneCallback += commands.Execute;
                commands.EndCallback -= Complete;
                commands.EndCallback += Complete;
            }
        }
        protected int reservedFirst;
        protected int reservedLast;
        public abstract bool GetCommands(out CommandActionList<ICommand> commands);
        public virtual void InsertFirst(ICommand command) {
            if (GetCommands(out CommandActionList<ICommand> commands)) {
                commands.Insert(0, command); //insere na posicao atual reservada (se nao tiver nenhum reservado posicao 0
                reservedFirst++; //incrementa o reservado
            }
        }
        public virtual void InsertLast(ICommand command) {
            if (GetCommands(out CommandActionList<ICommand> commands)) {
                commands.Insert(commands.Count, command);
                reservedLast++;
            }
        }
        public virtual void Insert(ICommand command) {
            if (GetCommands(out CommandActionList<ICommand> commands)) {
                int f = reservedFirst;//0    1
                int index = commands.Count - reservedLast; //0 - 0  2 - 0 = 2 insere posicao 2     2 - 1 == 1 insere posicao 1
                int i = Mathf.Clamp(index, f, index); //esse clamp eh soh pra ter certeza q nunca vai ser menor q o "f" e nunca maior que o index
                commands.Insert(i, command);
            }
        }
    }
    public abstract class CommandEventInfo<T> : CommandEventInfo where T : ICommand {
        public abstract bool GetCommands(out CommandActionList<T> commands);
        public virtual void InsertFirst(T command) => InsertFirst((ICommand)command);
        public virtual void InsertLast(T command) => InsertLast((ICommand)command);
        public virtual void Insert(T command) => Insert((ICommand)command);
    }
    [Obsolete("Use new CommandEventInfo")] public abstract class CommandInfo : EventInfo, ICommandEvent {
        public CommandInfo(CommandActionList<ICommand> commands) {
            reservedFirst = 0;
            reservedLast = 0;
            _commands = commands;
        }
        private CommandActionList<ICommand> _commands;
        private int reservedFirst;
        private int reservedLast;
        public void InsertFirst(ICommand command) {
            _commands.Insert(0, command); //insere na posicao atual reservada (se nao tiver nenhum reservado posicao 0
            reservedFirst++; //incrementa o reservado
        }
        public void InsertLast(ICommand command) {
            _commands.Insert(_commands.Count, command);
            reservedLast++;
        }
        public void Insert(ICommand command) {
            int f = reservedFirst;//0    1
            int index = _commands.Count - reservedLast; //0 - 0  2 - 0 = 2 insere posicao 2     2 - 1 == 1 insere posicao 1
            int i = Mathf.Clamp(index, f, index); //esse clamp eh soh pra ter certeza q nunca vai ser menor q o "f" e nunca maior que o index
            _commands.Insert(i, command);
        }
    }
    [Obsolete("Use new CommandEventInfo")] public abstract class CommandInfo<T> : EventInfo where T : ICommand {
        public CommandInfo(CommandActionList<T> commands) {
            reservedFirst = 0;
            reservedLast = 0;
            _commands = commands;
        }
        private CommandActionList<T> _commands;
        private int reservedFirst;
        private int reservedLast;
        public void InsertFirst(T command) {
            _commands.Insert(0, command); //insere na posicao atual reservada (se nao tiver nenhum reservado posicao 0
            reservedFirst++; //incrementa o reservado
        }
        public void InsertLast(T command) {
            _commands.Insert(_commands.Count, command);
            reservedLast++;
        }
        public void Insert(T command) {
            int f = reservedFirst;//0    1
            int index = _commands.Count - reservedLast; //0 - 0  2 - 0 = 2 insere posicao 2     2 - 1 == 1 insere posicao 1
            int i = Mathf.Clamp(index, f, index); //esse clamp eh soh pra ter certeza q nunca vai ser menor q o "f" e nunca maior que o index
            _commands.Insert(i, command);
        }
    }
}