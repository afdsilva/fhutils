using System;
using System.Collections.Generic;

namespace FourHands {
    public abstract class EventInfo {
        public string Name;
        /// <summary>
        /// When Complete is called CompleteCallback will run it can be used in conjunction with DoneCallback
        /// to run code after Done is complete, otherwise will not be used
        /// </summary>
        public virtual event Action CompleteCallback;
        public virtual event Action DoneCallback;
        public void Complete() {
            CompleteCallback?.Invoke();
        }

        /// <summary>
        /// Done funciona meio que assincronamente, mesmo que o Done() seja chamado, ele ir� verificar se todas
        /// chamadas de WaitFor j� terminaram, asim somente a �ltima chamada de Done ir� executar o callback;
        /// </summary>
        /// <param name="id"></param>
        //Dictionary<string, bool> waiting;
        Dictionary<object, bool> semaphore;
        public void WaitFor(object lockObj) {
            if (semaphore == null) semaphore = new Dictionary<object, bool>();
            if (semaphore.ContainsKey(lockObj)) throw new Exception("Key already locked.");
            semaphore[lockObj] = false;
        }
        bool isDone;
        public void Done() {
            if (semaphore == null) semaphore = new Dictionary<object, bool>();
            if (isDone) return;
            foreach (var item in semaphore) {
                if (item.Value == false) return;
            }
            isDone = true;
            DoneCallback?.Invoke();
        }
        public void Done(object lockObj) {
            if (semaphore == null) semaphore = new Dictionary<object, bool>();
            if (lockObj != null) {
                if (!semaphore.ContainsKey(lockObj)) throw new Exception("Key not found.");
                semaphore[lockObj] = true;
            }
            Done();
        }
    }
    public abstract class UnitEventInfo<U> : EventInfo where U : IUnit {
        public UnitEventInfo(U instance) {
            Instance = instance;
        }
        public U Instance { get; protected set; }
    }
}