﻿/// <summary>
/// Source:
/// https://codewithshadman.com/state-pattern-csharp/
/// https://refactoring.guru/pt-br/design-patterns/state/csharp/example
/// </summary>
namespace FourHands {
    // The BaseContext defines the interface of interest to clients. It also
    // maintains a reference to an instance of a State subclass, which
    // represents the current state of the Context.
    public abstract class BaseContext {
        protected abstract void setState(BaseStatePattern state);
    }
    public abstract class BaseStatePattern {
        public BaseStatePattern(BaseContext context) {
            _context = context;
        }
        protected BaseContext _context;
    }
}