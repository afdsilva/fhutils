﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Linq;
using System.Reflection;

namespace FourHands {
    public static class FHUtility {

        #region Generic
        public static bool DebugMode;
        public static bool GetClassMethod(string typeName, string methodName, out Type classType, out MethodInfo methodInfo) {
            classType = null;
            methodInfo = null;
            try {
                classType = GetType(typeName);
                if (classType == null) {
                    Debug.LogWarning("typeName = " + typeName + " not found.");
                    return false;
                }
                methodInfo = classType.GetMethod(methodName);
                if (methodInfo == null) {
                    Debug.LogWarning("methodName " + methodName + " not found.");
                    return false;
                }
                return true;
            } catch (Exception e) {
                Debug.LogWarning(e.Message);
                return false;
            }
        }
        public static bool GetClassMethod(string eventName, out Type classType, out MethodInfo methodInfo) {
            classType = null;
            methodInfo = null;
            if (eventName.Contains(".") == false) {
                return false;
            }
            string[] cbStringArray = eventName.Split('.');
            string classString = cbStringArray.Length > 0 ? cbStringArray[0] : null;
            classType = Type.GetType(classString);
            if (classType == null) {
                //Debug.LogError("Event::GetEventClassMethod::classType::" + classString + " not found.");
                return false;
            }
            string methodString = cbStringArray.Length > 1 ? cbStringArray[1] : null;
            methodInfo = classType.GetMethod(methodString);
            if (methodInfo == null) {
                //Debug.LogError("Event::GetEventClassMethod::methodInfo::" + methodString + " not found.");
                return false;
            }
            return true;

        }
        public static Type GetType(string typeName) {
            if (string.IsNullOrEmpty(typeName) == true)
                return null;
            var type = Type.GetType(typeName);
            if (type != null) return type;
            foreach (var a in AppDomain.CurrentDomain.GetAssemblies()) {
                type = a.GetType(typeName);
                if (type != null)
                    return type;
            }
            return null;
        }

        public static float MapRange(float x, float a, float b, float c, float d) {
            if (DebugMode) Debug.Log($"x: {x} a: {a} b: {b} c: {c} d: {d}");
            //float y = ((x - a) * ((d - c) / (b - a))) + c;
            return ((x - a) * ((d - c) / (b - a))) + c;
        }

        public static Vector2 GetMouseWorld2D() {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            return new Vector2(pos.x, pos.y);
        }
        public static Vector2 GetScreenWorld2D(Camera camera, Vector3 screenPoint) {
            Vector3 pos = camera.ScreenToWorldPoint(screenPoint);
            return new Vector2(pos.x, pos.y);
        }
        public static Vector2 GetScreenWorld2D(Vector3 screenPoint) {
            Vector3 pos = Camera.main.ScreenToWorldPoint(screenPoint);
            return new Vector2(pos.x, pos.y);
        }
        #endregion
        #region Security
        public static class StringCipher {
            // This constant is used to determine the keysize of the encryption algorithm in bits.
            // We divide this by 8 within the code below to get the equivalent number of bytes.
            private const int Keysize = 256;

            // This constant determines the number of iterations for the password bytes generation function.
            private const int DerivationIterations = 1000;

            public static string Encrypt(string plainText, string passPhrase) {
                // Salt and IV is randomly generated each time, but is preprended to encrypted cipher text
                // so that the same Salt and IV values can be used when decrypting.  
                var saltStringBytes = Generate256BitsOfRandomEntropy();
                var ivStringBytes = Generate256BitsOfRandomEntropy();
                var plainTextBytes = Encoding.UTF8.GetBytes(plainText);

                var password = new Rfc2898DeriveBytes(passPhrase, saltStringBytes, DerivationIterations);
                var keyBytes = password.GetBytes(Keysize / 8);
                using (var symmetricKey = new RijndaelManaged()) {
                    symmetricKey.BlockSize = 256;
                    symmetricKey.Mode = CipherMode.CBC;
                    symmetricKey.Padding = PaddingMode.PKCS7;
                    using (var encryptor = symmetricKey.CreateEncryptor(keyBytes, ivStringBytes)) {
                        using (var memoryStream = new MemoryStream()) {
                            using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write)) {
                                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                cryptoStream.FlushFinalBlock();
                                // Create the final bytes as a concatenation of the random salt bytes, the random iv bytes and the cipher bytes.
                                var cipherTextBytes = saltStringBytes;
                                cipherTextBytes = cipherTextBytes.Concat(ivStringBytes).ToArray();
                                cipherTextBytes = cipherTextBytes.Concat(memoryStream.ToArray()).ToArray();
                                memoryStream.Close();
                                cryptoStream.Close();
                                return Convert.ToBase64String(cipherTextBytes);
                            }
                        }
                    }
                }

            }

            public static string Decrypt(string cipherText, string passPhrase) {
                // Get the complete stream of bytes that represent:
                // [32 bytes of Salt] + [32 bytes of IV] + [n bytes of CipherText]
                var cipherTextBytesWithSaltAndIv = Convert.FromBase64String(cipherText);
                // Get the saltbytes by extracting the first 32 bytes from the supplied cipherText bytes.
                var saltStringBytes = cipherTextBytesWithSaltAndIv.Take(Keysize / 8).ToArray();
                // Get the IV bytes by extracting the next 32 bytes from the supplied cipherText bytes.
                var ivStringBytes = cipherTextBytesWithSaltAndIv.Skip(Keysize / 8).Take(Keysize / 8).ToArray();
                // Get the actual cipher text bytes by removing the first 64 bytes from the cipherText string.
                var cipherTextBytes = cipherTextBytesWithSaltAndIv.Skip((Keysize / 8) * 2).Take(cipherTextBytesWithSaltAndIv.Length - ((Keysize / 8) * 2)).ToArray();

                var password = new Rfc2898DeriveBytes(passPhrase, saltStringBytes, DerivationIterations);
                var keyBytes = password.GetBytes(Keysize / 8);
                using (var symmetricKey = new RijndaelManaged()) {
                    symmetricKey.BlockSize = 256;
                    symmetricKey.Mode = CipherMode.CBC;
                    symmetricKey.Padding = PaddingMode.PKCS7;
                    using (var decryptor = symmetricKey.CreateDecryptor(keyBytes, ivStringBytes)) {
                        using (var memoryStream = new MemoryStream(cipherTextBytes)) {
                            using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read)) {
                                var plainTextBytes = new byte[cipherTextBytes.Length];
                                var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                                memoryStream.Close();
                                cryptoStream.Close();
                                return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                            }
                        }
                    }
                }

            }

            private static byte[] Generate256BitsOfRandomEntropy() {
                var randomBytes = new byte[32]; // 32 Bytes will give us 256 bits.
                var rngCsp = new RNGCryptoServiceProvider();
                // Fill the array with cryptographically secure random bytes.
                rngCsp.GetBytes(randomBytes);
                return randomBytes;
            }
        }
        #endregion
    }
    [Obsolete("Usar UpdateTimer")]
    public class FunctionTimer {
        protected FunctionTimer(Action<float> action, float timer, string id = null) {
            ID = id;
            timedAction = action;
            MaxTicks = TimeTickSystem.GetTickSeconds(timer);
            ElapsedTime = 0;
        }

        public static List<FunctionTimer> activeTimers;
        public string ID { get; protected set; }
        Action<float> timedAction;
        public int MaxTicks { get; protected set; }
        public int CurrentTick { get; protected set; }
        public float ElapsedTime { get; protected set; }
        protected void startTimer() {
            TimeTickSystem.OnTick += functionTimerTickCallback;
        }
        protected void stopTimer() {
            TimeTickSystem.OnTick -= functionTimerTickCallback;
            remove(this);
        }
        protected void cancelTimer() {
            stopTimer();
        }
        protected void finishTimer() {
            stopTimer();
            timedAction?.Invoke(ElapsedTime);
        }
        private void functionTimerTickCallback(object sender, TimeTickSystem.OnTickEventArgs e) {
            ElapsedTime += e.deltaTime;
            CurrentTick++;
            if (CurrentTick >= MaxTicks) {
                CurrentTick = MaxTicks;
                finishTimer();
            }
        }
        [Obsolete("Use timed Version")]public static FunctionTimer Create(Action action, float timer, string id = null) {

            return Create((f) => { action(); }, timer, id);
        }
        public static FunctionTimer Create(Action<float> action, float timer, string id = null) {
            if (activeTimers == null) activeTimers = new List<FunctionTimer>();
            if (id == null) {
                id = $"nameless_{activeTimers.Count + 1}_{DateTime.Now}";
            }
            FunctionTimer functionTimer = new FunctionTimer(action, timer, id);
            functionTimer.startTimer();
            activeTimers.Add(functionTimer);
            return functionTimer;
        }

        protected static bool remove(FunctionTimer timer) {
            if (activeTimers == null) activeTimers = new List<FunctionTimer>();
            return activeTimers.Remove(timer);
        }
        public static void CancelTimer(string id) {
            for (int i = 0; i < activeTimers.Count; i++) {
                if (activeTimers[i].ID == id) {
                    activeTimers[i].cancelTimer();
                    i--;
                }
            }
        }
        public static void FinishTimer(string id) {
            for (int i = 0; i < activeTimers.Count; i++) {
                if (activeTimers[i].ID == id) {
                    activeTimers[i].finishTimer();
                    i--;
                }
            }
        }
    }
    public class UpdateTimer : IUnit {
        protected UpdateTimer(float timer, Action<float> updateCallback, Action endCallback, bool useUnscaledTime = false) {
            totalTimer = timer;
            this.updateCallback = updateCallback;
            this.endCallback = endCallback;
            this.useUnscaledTime = useUnscaledTime;
        }

        [Obsolete("only the internal endCallback function")]public event Action Completed;

        float totalTimer;
        float timer;
        Action<float> updateCallback;
        Action endCallback;
        bool useUnscaledTime;
        bool isPaused;
        public bool IsCompleteOrCanceled { get; protected set; }

        protected void startTimer() {
            timer = 0;
            IsCompleteOrCanceled = false;
            ObserverEventSystem<UpdateTimerEvent>.RegisterListener(update);
            UnitObserverEventSystem<PauseTimeEvent, UpdateTimer>.RegisterListener(pauseTimer, this);
            UnitObserverEventSystem<ResumeTimeEvent, UpdateTimer>.RegisterListener(resumeTimer, this);
        }

        void update(UpdateTimerEvent updateTimerEvent) {
            if (isPaused) return;
            if (useUnscaledTime) {
                timer += updateTimerEvent.UnscaledDeltaTime;
            } else {
                timer += updateTimerEvent.DeltaTime;
            }
            if (timer >= totalTimer) timer = totalTimer;
            float perc = totalTimer <= 0 ? 1f : timer / totalTimer;
            updateCallback?.Invoke(perc);
            if (perc >= 1) stopTimer();
        }
        void pauseTimer(PauseTimeEvent pauseEvent) {
            isPaused = true;
        }
        void resumeTimer(ResumeTimeEvent resumeEvent) {
            isPaused = false;
        }
        protected void stopTimer() {
            clearCallbacks();
            endCallback?.Invoke();
        }
        protected void cancelTimer() {
            clearCallbacks();
        }
        void clearCallbacks() {
            UnitObserverEventSystem<PauseTimeEvent, UpdateTimer>.UnregisterListener(pauseTimer, this);
            UnitObserverEventSystem<ResumeTimeEvent, UpdateTimer>.UnregisterListener(resumeTimer, this);
            ObserverEventSystem<UpdateTimerEvent>.UnregisterListener(update);
            IsCompleteOrCanceled = true;
        }
        internal class UpdateTimerEvent : EventInfo {
            public UpdateTimerEvent(float deltaTime, float unscaledDeltaTime) {
                DeltaTime = deltaTime;
                UnscaledDeltaTime = unscaledDeltaTime;
            }
            public float DeltaTime { get; protected set; }
            public float UnscaledDeltaTime { get; protected set; }
        }
        internal class PauseTimeEvent : UnitEventInfo<UpdateTimer> {
            public PauseTimeEvent(UpdateTimer instance) : base(instance) { }
        }
        internal class ResumeTimeEvent : UnitEventInfo<UpdateTimer> {
            public ResumeTimeEvent(UpdateTimer instance) : base(instance) { }
        }
        internal class UpdateTimerHook : MonoBehaviour {
            protected static UpdateTimerHook _instance;
            
            public static void Init() {
                if (_instance == null) _instance = (new GameObject("UpdateTimerHook")).AddComponent<UpdateTimerHook>();
            }
            private void Update() {
                ObserverEventSystem<UpdateTimerEvent>.Trigger(new UpdateTimerEvent(Time.deltaTime, Time.unscaledDeltaTime));
            }
        }
        public static UpdateTimer Create(float timer, Action<float> updateCallback, Action endCallback, bool useUnscaledTime = false) {
            UpdateTimerHook.Init();
            UpdateTimer updateTimer = new UpdateTimer(timer, updateCallback, endCallback, useUnscaledTime);
            updateTimer.startTimer();
            return updateTimer;
        }
        public static UpdateTimer Create(float timer, Action endCallback = null, bool useUnscaledTime = false) {
            UpdateTimerHook.Init();
            UpdateTimer updateTimer = new UpdateTimer(timer, null, endCallback, useUnscaledTime);
            updateTimer.startTimer();
            return updateTimer;
        }
        public static void StopTimer(UpdateTimer updateTimer) {
            updateTimer?.stopTimer();
        }
        public static void CancelTimer(UpdateTimer updateTimer) {
            updateTimer?.cancelTimer();
        }
        public static void PauseTimer(UpdateTimer updateTimer) {
            if (updateTimer == null) return;
            UnitObserverEventSystem<PauseTimeEvent, UpdateTimer>.Trigger(new PauseTimeEvent(updateTimer));
        }
        public static void ResumeTimer(UpdateTimer updateTimer) {
            if (updateTimer == null) return;
            UnitObserverEventSystem<ResumeTimeEvent, UpdateTimer>.Trigger(new ResumeTimeEvent(updateTimer));
        }
    }
}