﻿using UnityEngine;
using System.Collections.Generic;
namespace FourHands {
    public interface IId {
        int GetID();
        void SetID(int newID);
        string GetKey();
        void SetKey(string newKey);
    }
    public class GenerateID<T> where T : IId {
        static protected GenerateID<T> _instance;
        static public GenerateID<T> Instance {
            get {
                if (_instance == null)
                    _instance = new GenerateID<T>();
                return _instance;
            }
        }

        protected Dictionary<int, T> processedIDs = new Dictionary<int, T>();
        public static bool NextID(T data, out string error) {
            error = string.Empty;
            if (checkProcessedData(data, out error) == false) {
                //se o dado ja foi processado sai fora
                error = "ID already processed.";
                return false;
            }
            int nextID = 0;
            while (CheckUniqueID(nextID) == false) {
                nextID++;
            }

            data.SetID(nextID);
            Instance.processedIDs[nextID] = data;
            return true;
        }
        public static bool CheckUniqueID(int id) {
            
            return Instance.processedIDs.ContainsKey(id);

            //foreach (var item in Instance.processedIDs) {
            //    if (item.GetID() == id)
            //        return false;
            //}
            //return true;
        }
        protected static bool checkProcessedData(T data, out string error) {
            error = null;
            if (data == null) {
                throw new System.NullReferenceException("GenerateID<" + typeof(T).FullName + ">::checkUniqueData::data NULL.");
            }
            //pra cada id ja processado, verifica se a mesma chave já esta registrada, se sim, retorna verdadeiro
            foreach (var item in Instance.processedIDs) {
                if (item.Value.GetKey() == data.GetKey()) return true;
            }
            return false;
        }
        public static bool ProcessID(T data, out string error) {
            error = null;
            if (data == null)
                throw new System.Exception("GenerateID<" + typeof(T).FullName + ">.ProcessID::Data NULL.");
            if (data.GetID() == -1) {
                error = "Can't process -1 IDs";
                return false;
            }

            if (checkProcessedData(data, out error) == true) {
                //dado ja processado retorna falso
                error = "Data already processed.";
                return false;
            }
            if (Instance.processedIDs.ContainsKey(data.GetID()) == true) {
                error = "ID already in use.";
                return false;
            }
            Instance.processedIDs[data.GetID()] = data;
            return true;
        }
    }
}