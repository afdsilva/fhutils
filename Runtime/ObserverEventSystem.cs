﻿using System.Collections.Generic;
using System;

namespace FourHands {
    public static class ObserverEventSystem {
        public static T Trigger<T>(T eventInfo) where T : EventInfo {
            if (eventInfo == null) throw new NullReferenceException("ObserverEventSystem::Event null.");
            ObserverEventSystem<T>.Trigger(eventInfo);
            return eventInfo;
        }
        public static void RegisterListener<T>(Action<T> listener, bool onlyOnceInstance = false) where T : EventInfo {
            //ObserverEventSystem<T>.eventListeners += listener;
            ObserverEventSystem<T>.RegisterListener(listener, onlyOnceInstance);
        }
        public static void UnregisterListener<T>(Action<T> listener) where T : EventInfo {
            ObserverEventSystem<T>.UnregisterListener(listener);
        }
        public static void ClearListener<T>() where T : EventInfo {
            ObserverEventSystem<T>.ClearListener();
        }
    }
    public static class ObserverEventSystem<T> where T : EventInfo {
        static event Action<T> eventListeners;
        public static void RegisterListener(Action<T> listener, bool onlyOnceInstance = false) {
            if (listener == null) throw new NullReferenceException($"Listener cannot be null");
            if (onlyOnceInstance) eventListeners -= listener;
            eventListeners += listener;
        }
        public static void UnregisterListener(Action<T> listener) {
            if (listener == null) throw new NullReferenceException($"Listener cannot be null");
            eventListeners -= listener;
        }
        public static void ClearListener() {
            eventListeners = null;
        }
        public static T Trigger(T eventInfo) {
            if (eventInfo == null) throw new NullReferenceException("ObserverEventSystem::Event null.");
            if (eventListeners != null) {
                foreach (Action<T> listener in eventListeners.GetInvocationList()) {
                    listener.Invoke(eventInfo);
                }
            }
            return eventInfo;
        }
    }
    public static class UnitObserverEventSystem {
        public static T Trigger<T, L>(T eventInfo) where T : UnitEventInfo<L> where L : IUnit {
            UnitObserverEventSystem<T, L>.Trigger(eventInfo);
            return eventInfo;
        }
        public static void RegisterListener<T, L>(Action<T> listener, L instance, bool onlyOnceInstance = false) where T : UnitEventInfo<L> where L : IUnit {
            UnitObserverEventSystem<T, L>.RegisterListener(listener, instance, onlyOnceInstance);
        }
        public static void UnregisterListener<T, L>(Action<T> listener, L instance) where T : UnitEventInfo<L> where L : IUnit {
            UnitObserverEventSystem<T, L>.UnregisterListener(listener, instance);
        }
    }
    public static class UnitObserverEventSystem<T, L> where T : UnitEventInfo<L> where L : IUnit {
        static Dictionary<L, Action<T>> eventListeners;

        public static T Trigger(T eventInfo) {
            if (eventInfo == null) throw new NullReferenceException("ObserverEventSystem::Event null.");
            if (eventListeners == null) eventListeners = new Dictionary<L, Action<T>>();

            if (eventListeners.ContainsKey(eventInfo.Instance) && eventListeners[eventInfo.Instance] != null) {
                foreach (Action<T> item in eventListeners[eventInfo.Instance].GetInvocationList()) {
                    item?.Invoke(eventInfo);
                }
            }
            return eventInfo;
        }
        public static void RegisterListener(Action<T> listener, L unit, bool onlyOnceInstance = false) {
            if (eventListeners == null) eventListeners = new Dictionary<L, Action<T>>();
            if (eventListeners.ContainsKey(unit) == false) eventListeners.Add(unit, null);
            if (onlyOnceInstance) eventListeners[unit] -= listener;
            eventListeners[unit] += listener;
        }
        public static void UnregisterListener(Action<T> listener, L unit) {
            if (eventListeners == null) eventListeners = new Dictionary<L, Action<T>>();
            if (eventListeners.ContainsKey(unit) == false) eventListeners.Add(unit, null);
            eventListeners[unit] -= listener;
        }
    }
}