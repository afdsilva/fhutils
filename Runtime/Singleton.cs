﻿using System;
using System.Collections;
using UnityEngine;

namespace FourHands {
    public abstract class Singleton : MonoBehaviour {
        protected internal Func<IEnumerator> _onInitialize;
        protected internal Func<IEnumerator> _onModuleInitialize;
        protected internal Func<IEnumerator> _onModuleComplete; 
        protected internal Func<IEnumerator> _onComplete; //Usado quando 

        bool init = false;
        protected IEnumerator Initialize() {
            if (init == true) yield break;

            yield return OnInitializeCallback();

            yield return OnModuleInitializeCallback();
            yield return OnModuleCompleteCallback();

            yield return OnCompleteCallback();

            init = true;

            yield break;
        }
        public abstract IEnumerator OnInitializeCallback();
        public abstract IEnumerator OnModuleInitializeCallback();
        public abstract IEnumerator OnModuleCompleteCallback();
        public abstract IEnumerator OnCompleteCallback();
        protected abstract void registerModule(Singleton singleton);
        public virtual bool IsRegistered { get; set; }

    }
    /// <summary>
    /// Be aware this will not prevent a non singleton constructor
    ///   such as `T myT = new T();`
    /// To prevent that, add `protected T () {}` to your singleton class.
    /// 
    /// As a note, this is made as MonoBehaviour because we need Coroutines.
    /// </summary>
    ///
    public abstract class Singleton<T> : Singleton where T : Singleton {
        private static T _instance;
        private static object _lock = new object();
        public static T Instance {
            get {
                if (applicationIsQuitting) {
                    //Debug.LogWarning("[Singleton] Instance '" + typeof(T) + "' already destroyed on application quit." + " Won't create again - returning null.");
                    if (Application.isPlaying == true)
                        return null;
                }

                lock (_lock) {
                    if (_instance == null) {
                        _instance = (T)FindObjectOfType(typeof(T));

                        if (FindObjectsOfType(typeof(T)).Length > 1) {
                            //Debug.LogError("[Singleton] Something went really wrong " + " - there should never be more than 1 singleton!" + " Reopening the scene might fix it.");
                            return _instance;
                        }

                        if (_instance == null) {
                            if (Application.isPlaying == false) {
                                //Debug.Log("Acessing script in Editor");
                                return null;
                            }
                            GameObject singleton = new GameObject();
                            _instance = singleton.AddComponent<T>();
                            singleton.name = "(singleton) " + typeof(T).ToString();
                            if (Application.isPlaying == true)
                                DontDestroyOnLoad(singleton);
                            //Debug.Log("[Singleton] An instance of " + typeof(T) + " is needed in the scene, so '" + singleton + "' was created with DontDestroyOnLoad.");
                        } else {
                            //Debug.Log("[Singleton] Using instance already created: " + _instance.gameObject.name);
                        }

                    }

                    return _instance;
                }
            }
        }
        protected static bool applicationIsQuitting = false;
        private static object _evenLock = new object();
        /// <summary>
        /// When Unity quits, it destroys objects in a random order.
        /// In principle, a Singleton is only destroyed when application quits.
        /// If any script calls Instance after it have been destroyed, 
        ///   it will create a buggy ghost object that will stay on the Editor scene
        ///   even after stopping playing the Application. Really bad!
        /// So, this was made to be sure we're not creating that buggy ghost object.
        /// </summary>
        private void Awake() {
            applicationIsQuitting = false;
            if (_instance == null) {
                //Debug.LogWarning($"{GetType().FullName} Awake::Creating");
                _instance = this as T;
                if (Application.isPlaying == true)
                    DontDestroyOnLoad(gameObject);
            } else {
                //Debug.LogWarning($"{GetType().FullName} Awake::Destroying");
                Destroy(gameObject);
                return;
            }

            SingletonAwake();
        }
        /// <summary>
        /// Instead of overriding Awake the singleton needs to implement SingletonAwake,
        /// This is be used in most cases to register the singleton in the main manager;
        /// Ex:
        ///     MainManager.OnInitialize += Initialize;
        /// The MainManager class need to call in it's Start the command the Init method;
        /// Ex:
        ///     yield return Initialize();
        /// </summary>
        public virtual void SingletonAwake() { }
        //public virtual void OnDestroy() {
        //    applicationIsQuitting = true;
        //}
        public virtual void OnApplicationQuit() {
            if (IsRegistered == false) {
                Debug.LogWarning(GetType().FullName + " Module was never registered.");
            }

            applicationIsQuitting = true;
        }
        public static event Func<IEnumerator> OnInitialize {
            add { Instance._onInitialize += value; }
            remove { Instance._onInitialize -= value; }
        }
        //module so sera acessado pelo OnModuleInitializeCallback
        public static event Func<IEnumerator> OnModuleLoad {
            add { Instance._onModuleComplete += value; }
            remove { Instance._onModuleComplete -= value; }
        }
        public static event Func<IEnumerator> OnComplete {
            add { Instance._onComplete += value; }
            remove { Instance._onComplete -= value; }
        }
        public override IEnumerator OnInitializeCallback() {
            //Debug.Log(GetType().FullName + "::OnInitializeCallback");
            if (_onInitialize == null) yield break;
            foreach (Func<IEnumerator> item in _onInitialize.GetInvocationList()) {
                yield return item.Invoke();
            }
            
        }
        public override IEnumerator OnModuleInitializeCallback() {
            //Debug.Log(GetType().FullName + "::OnModuleInitializeCallback");
            if (_onModuleInitialize == null) yield break;
            foreach (Func<IEnumerator> item in _onModuleInitialize.GetInvocationList()) {
                yield return item.Invoke();
            }
            yield break;
        }
        public override IEnumerator OnModuleCompleteCallback() {
            //Debug.Log(GetType().FullName + "::OnModuleCallback");
            if (_onModuleComplete == null) yield break;
            foreach (Func<IEnumerator> item in _onModuleComplete.GetInvocationList()) {
                yield return item.Invoke();
            }
            yield break;
        }
        public override IEnumerator OnCompleteCallback() {
            //Debug.Log("Singleton::OnCompleteCallback");
            if (_onComplete == null) yield break;
            foreach (Func<IEnumerator> item in _onComplete.GetInvocationList()) {
                yield return item.Invoke();
            }
            yield break;
        }
        protected override void registerModule(Singleton singleton) {
            if (singleton == null) return;
            singleton.IsRegistered = true;
            //Debug.Log(GetType().FullName + "::RegisterModule::" + singleton.GetType().FullName);
            _onModuleInitialize += singleton.OnInitializeCallback;
            _onModuleComplete += singleton.OnCompleteCallback;
        }

    }
}