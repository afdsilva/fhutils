﻿using System;
using System.Collections.Generic;

namespace FourHands {
    /// <summary>
    /// BASE: Building Game Bots in Unity with State Machines
    /// https://www.youtube.com/watch?v=V75hgcsCGOM&t=0s
    public class StateMachine {
        private IState _currentState;
        private Dictionary<Type, List<Transition>> _transitions = new Dictionary<Type, List<Transition>>();
        private List<Transition> _currentTransitions = new List<Transition>();
        private List<Transition> _anyTransitions = new List<Transition>();
        private static List<Transition> EmptyTransitions = new List<Transition>(0);

        public void Tick() {
            Transition transition = getTransition();
            if (transition != null) SetState(transition?.To);
            _currentState?.Update();

        }
        public void SetState(IState state) {
            if (state == _currentState) return;
            _currentState?.OnExit();
            _currentState = state;

            if (_currentState == null) throw new NullReferenceException($"{GetType().Name}::SetState::State cannot be null.");
            _transitions.TryGetValue(_currentState.GetType(), out _currentTransitions);
            if (_currentTransitions == null) _currentTransitions = EmptyTransitions;
            _currentState.OnEnter();
        }
        public void AddTransition(IState from, IState to, Func<IState, bool> predicate) {
            if (_transitions.TryGetValue(from.GetType(), out var transitions) == false) {
                transitions = new List<Transition>();
                _transitions[from.GetType()] = transitions;
            }
            transitions.Add(new Transition(to, predicate));
        }
        public void AddAnyTransition(IState state, Func<bool> predicate, bool useInstance = true) {
            _anyTransitions.Add(new Transition(state, predicate));
        }

        private class Transition {
            public Func<IState, bool> Condition { get; }
            public Func<bool> AnyCondition { get; }
            public IState To { get; }

            public Transition(IState to, Func<IState, bool> condition) {
                To = to;
                Condition = condition;
            }
            public Transition(IState to, Func<bool> anyCondition) {
                To = to;
                AnyCondition = anyCondition;
            }
        }
        private Transition getTransition() {
            foreach (var transition in _anyTransitions) {
                if (transition.AnyCondition()) return transition;
            }
            if (_currentState == null) return null;
            foreach (var transition in _currentTransitions) {
                if (transition.Condition(_currentState)) return transition;
            }
            return null;
        }
    }
    public interface IState {
        void Update();
        void OnEnter();
        void OnExit();
        bool IsDone { get; }
    }
}