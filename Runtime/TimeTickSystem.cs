﻿using System;
using UnityEngine;

namespace FourHands {
    public class TimeTickSystem : Singleton<TimeTickSystem> {
        public class OnTickEventArgs : EventArgs {
            public int tick;
            public float deltaTime;
        }
        public static event EventHandler<OnTickEventArgs> OnTick;
        private const float TICK_TIMER_MAX = 0.02f;
        private int tick;
        private float tickTimer;

        public override void SingletonAwake() {
            tick = 0;
        }
        private void Update() {
            float deltaTime = Time.deltaTime;
            tickTimer += deltaTime;
            if (tickTimer >= TICK_TIMER_MAX) {
                tickTimer -= TICK_TIMER_MAX;
                tick++;
                OnTick?.Invoke(this, new OnTickEventArgs { tick = tick, deltaTime = deltaTime });
            }
        }
        public static int GetTickSeconds(float delay) {
            
            return Mathf.CeilToInt(delay / TICK_TIMER_MAX);
        }
    }
}